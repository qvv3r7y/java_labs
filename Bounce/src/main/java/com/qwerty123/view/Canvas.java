package com.qwerty123.view;

import com.qwerty123.KeyController;
import com.qwerty123.model.Obstacle;
import com.qwerty123.model.ResourceManager;
import com.qwerty123.model.Sprite;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Canvas extends JPanel {
    ResourceManager resourceManager;
    Font font = new Font("Times New Roman", Font.PLAIN, 15);

    public Canvas(ResourceManager resourceManager, KeyController keyController) {
        this.resourceManager = resourceManager;
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                keyController.keyPressed(e);
            }
        });
        setFocusable(true);
    }

    @Override
    public void paint(Graphics g) {

        switch (resourceManager.getContext()) {
            case IN_MENU:
                drawInMenu(g);
                break;
            case IN_GAME:
                drawInGame(g);
                break;
            case GAME_OVER:
                drawGameOver(g);
                break;
        }

        Toolkit.getDefaultToolkit().sync();
    }

    private void drawGameOver(Graphics g) {
        g.drawImage(new ImageIcon("assets/heart_broken.png").getImage(), 0, 0, 50, 50, null);
    }

    private void drawInMenu(Graphics g) {
        paintAdapter(resourceManager.getStartMenu(), g);

        g.setFont(font);
        g.setColor(Color.BLACK);
        g.drawString("HI: " + resourceManager.getRecordDistance(), 930, 30);
    }

    private void drawInGame(Graphics g) {

        g.setFont(font);
        g.setColor(Color.BLACK);

        paintAdapter(resourceManager.getBackgroundsLayers()[0], g);
        paintAdapter(resourceManager.getBackgroundsLayers()[1], g);

        g.drawString("Dist: " + resourceManager.getDistance(), 915, 30);
        g.drawString("HI: " + resourceManager.getRecordDistance(), 815, 30);


        if (resourceManager.getCurLives() == 3) {
            g.drawImage(new ImageIcon("assets/heart.png").getImage(), 0, 0, 50, 50, null);
            g.drawImage(new ImageIcon("assets/heart.png").getImage(), 45, 0, 50, 50, null);
            g.drawImage(new ImageIcon("assets/heart.png").getImage(), 90, 0, 50, 50, null);
        } else if (resourceManager.getCurLives() == 2) {
            g.drawImage(new ImageIcon("assets/heart.png").getImage(), 0, 0, 50, 50, null);
            g.drawImage(new ImageIcon("assets/heart.png").getImage(), 45, 0, 50, 50, null);
            g.drawImage(new ImageIcon("assets/heart_broken.png").getImage(), 90, 0, 50, 50, null);
        } else if (resourceManager.getCurLives() == 1) {
            g.drawImage(new ImageIcon("assets/heart.png").getImage(), 0, 0, 50, 50, null);
            g.drawImage(new ImageIcon("assets/heart_broken.png").getImage(), 45, 0, 50, 50, null);
            g.drawImage(new ImageIcon("assets/heart_broken.png").getImage(), 90, 0, 50, 50, null);
        }

        //g.drawRect(resourceManager.getHero().getHitBox().x, resourceManager.getHero().getHitBox().y, resourceManager.getHero().getHitBox().width, resourceManager.getHero().getHitBox().height);

        for (Obstacle obstacle : resourceManager.getObstaclesList()) {
            paintAdapter(obstacle, g);
            //g.drawRect(obstacle.getHitBox().x, obstacle.getHitBox().y, obstacle.getHitBox().width, obstacle.getHitBox().height);
        }
        paintAdapter(resourceManager.getHero(), g);
    }

    private void paintAdapter(Sprite sprite, Graphics g) {

        g.drawImage(sprite.getCurrentImage(), sprite.getX(), sprite.getY(), sprite.getWidth(), sprite.getHeight(), null);
    }

    public void update() {
        repaint();
    }


}
