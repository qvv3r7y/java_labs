package com.qwerty123.model;

import java.awt.*;
import java.util.ArrayList;

public class Hero extends Sprite implements Interacting {
    public static final int HERO_X = 100;
    public static final int HERO_Y = 180;
    public static final int ACCELERATION_JUMP = -2;
    public static final int SPEED_JUMP = 29;
    public static final double SCALE = 0.875;

    private static int cadrCounter = 0;
    private boolean jumping = false;

    private ArrayList<Image> runImages;
    private ArrayList<Image> jumpImages;

    public Hero(ArrayList<Image> runImages, ArrayList<Image> jumpImages) {
        super(runImages.get(0));
        this.runImages = runImages;
        this.jumpImages = jumpImages;
        int width = (int) (runImages.get(0).getWidth(null) * SCALE);
        int height = (int) (runImages.get(0).getHeight(null) * SCALE);
        initParameters(HERO_X, HERO_Y, width, height, 0, SPEED_JUMP, 0, ACCELERATION_JUMP);
    }

    public void jump() {
        setCurrentImage(jumpImages, 2);
        if (y - speedY < HERO_Y) {
            y = y - speedY;
            speedY += accelerationY;
        } else {
            y = HERO_Y;
            speedY = SPEED_JUMP;
            setJumping(false);
        }
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public void setCurrentImage(ArrayList<Image> actionImage, int delay) {
        cadrCounter = cadrCounter % (actionImage.size() * delay);
        setCurrentImage(actionImage.get(cadrCounter / delay));
        cadrCounter++;
    }

    @Override
    public Rectangle getHitBox() {
        return new Rectangle(x + 45, y + 65, getWidth() - 85, getHeight() - 85);
    }

    @Override
    public void move() {
        setCurrentImage(runImages, 6);
    }
}
