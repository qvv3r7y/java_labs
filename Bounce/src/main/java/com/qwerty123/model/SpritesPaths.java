package com.qwerty123.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SpritesPaths {
    private Properties background = new Properties();
    private Properties heroes = new Properties();
    private Properties obstacles = new Properties();
    private Properties menu = new Properties();

    public SpritesPaths() {
        LoadSprites();
    }

    public void LoadSprites() {
        try {
            InputStream in = getClass().getClassLoader().getResourceAsStream("background_path.properties");
            background.load(in);
            in = getClass().getClassLoader().getResourceAsStream("heroes_path.properties");
            heroes.load(in);
            in = getClass().getClassLoader().getResourceAsStream("obstacles_path.properties");
            obstacles.load(in);
            in = getClass().getClassLoader().getResourceAsStream("menu_path.properties");
            menu.load(in);
        } catch (IOException | NullPointerException err) {
            throw new RuntimeException("File: " + "\"***.properties\"" + " not found", err);
        }
    }

    public Properties getBackground() {
        return background;
    }

    public Properties getHeroes() {
        return heroes;
    }

    public Properties getObstacles() {
        return obstacles;
    }

    public Properties getMenu() {
        return menu;
    }
}
