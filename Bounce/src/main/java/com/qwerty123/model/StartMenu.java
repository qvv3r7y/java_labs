package com.qwerty123.model;

import java.awt.*;
import java.util.ArrayList;

public class StartMenu extends Sprite {
    public static final double SCALE = 0.5;
    public static final int START_IMAGE = 2;

    private ArrayList<Image> menuImage;
    int curr = START_IMAGE;

    public StartMenu(ArrayList<Image> menuImage) {
        super(menuImage.get(START_IMAGE));
        this.menuImage = menuImage;
        int width = (int) (menuImage.get(0).getWidth(null) * SCALE);
        int height = (int) (menuImage.get(0).getHeight(null) * SCALE);
        initParameters(0, 0, width, height, 0, 0, 0, 0);
    }

    public void change(boolean isRight) {
        int tmp = curr;
        if (isRight == true) {
            tmp += 1;
        } else {
            tmp += -1;
        }
        if (tmp >= 0 && tmp < menuImage.size()) {
            curr = tmp;
            setCurrentImage(menuImage.get(curr));
        }
    }

    public String getChoose() {
        final String[] nameHeroes = {"woman_adventurer_", "robot_", "man_", "zombie_", "woman_", "man_adventurer_"};
        return nameHeroes[curr];
    }

    @Override
    public void move() {
    }
}
