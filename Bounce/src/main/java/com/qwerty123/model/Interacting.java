package com.qwerty123.model;

import java.awt.*;

public interface Interacting {
    Rectangle getHitBox();
}
