package com.qwerty123;

public abstract class ServiceOperation implements ICommand {

    @Override
    public boolean checkContext(Context context) {
        return context.sizeStack() >= 0;
    }
}
