package com.qwerty123;

public abstract class UnaryOperation implements ICommand {

    @Override
    public boolean checkContext(Context context) {
        return context.sizeStack() >= 1;
    }
}
