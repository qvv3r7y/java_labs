package com.qwerty123.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import com.qwerty123.ServiceOperation;

public class Print extends ServiceOperation {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(Arguments argv, Context context) {
        if (argv.size() != 0) {
            logger.warn(getClass().getName() + " have " + argv.size() + " args, but expected: 0");
        }
        System.out.println(context.peek());
    }

    @Override
    public boolean checkContext(Context context) {
        return context.sizeStack() >= 1;
    }
}
