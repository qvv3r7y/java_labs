package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class SqrtTest {
    private Context context;
    private Sqrt cmd;
    private static final String[] ARGS = "foo 3.14 10000000 -8".split(" ");
    private static final double DELTA = 1E-5;

    @Before
    public void initContext() {
        context = new Context();
        cmd = new Sqrt();
    }

    @Test
    public void sqrtTest() {
        context.push(123.123456789);
        cmd.execute(new Arguments(new String[]{" "}), context);
        Assert.assertEquals(Math.sqrt(123.123456789), context.pop(), DELTA);
    }

    @Test(expected = ArithmeticException.class)
    public void NegativeNumTest() {
        context.push(-123.123456789);
        cmd.execute(new Arguments(new String[]{" "}), context);
    }

    @Test
    public void badArgsTest() {
        context.push(9.0);
        context.push(1.0E+10);
        cmd.execute(new Arguments(ARGS), context);
        Assert.assertEquals(Math.sqrt(1.0E+10), context.pop(), DELTA);
    }
}
