package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class DivisionTest {
    private Context context;
    private Division cmd;
    private static final String[] ARGS = "foo 3.14 10000000 -8".split(" ");
    private static final double DELTA = 1E-5;

    @Before
    public void initContext() {
        context = new Context();
        cmd = new Division();
    }

    @Test
    public void DivTest() {
        context.push(-90.123456789);
        context.push(-0.1);
        cmd.execute(new Arguments(new String[]{" "}), context);
        Assert.assertEquals(-0.1 / -90.123456789, context.pop(), DELTA);
    }

    @Test(expected = ArithmeticException.class)
    public void DivByZeroTest() {
        context.push(-0.0);
        context.push(-90.123456789);
        cmd.execute(new Arguments(new String[]{" "}), context);
    }

    @Test
    public void badArgsTest() {
        context.push(9.0);
        context.push(1.0E+10);
        cmd.execute(new Arguments(ARGS), context);
        Assert.assertEquals(1.0E+10 / 9.0, context.pop(), DELTA);
    }
}
