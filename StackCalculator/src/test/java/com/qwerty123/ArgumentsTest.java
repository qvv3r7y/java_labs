package com.qwerty123;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class ArgumentsTest {
    private Arguments arguments;
    private static final String[] ARGS = "foo 3.14 10000000 -8".split(" ");

    @Before
    public void initArg() {
        arguments = new Arguments(ARGS);
    }

    @Test
    public void argsSize() {
        Assert.assertEquals(4, arguments.size());
    }

    @Test
    public void checkArgsCycle(){
        for (int i = 0; i < 3; i++){
            for (String arg : ARGS){
                Assert.assertEquals(arg, arguments.getNextArg());
            }
        }
    }

}
